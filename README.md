# Demo setup #

Under the `LiveAssistWithFCSDK/SessionManager` directory (i.e. in the `SessionManager` group), create a file called `Secrets.h`

* This file is `gitignore`d so changes won't appear in any change set

Add your specific configuration to `Secrets.h` in the form of `#define` statements, for example:

```
#define AUTHENTICATION_URL @"https://your.authentication.address/path/to/service"
#define GATEWAY_SCHEME @"https"
#define GATEWAY_ADDRESS @"your.gateway.address"
#define GATEWAY_PORT @"8443"
#define GATEWAY_CALLER @"assist-anon"
#define GATEWAY_CALLEE @"agent1"
```

### The Session Token Provider ###

To set up a FCSDK and LiveAssist session, a "session token" is required from a service _somewhere_. How this token is retrieved is outside the scope of the demo, but a backend sample has also been provided which works with the default implementation of `SessionTokenProvider` (called `RemoteAEDSessionTokenProvider`)

In a production system, be sure to use your own implementation of `SessionTokenProvider`.

### Caveats

This is a simple demo to show how FCSDK can be used inconjunction with Live Assist SDK to setup voice and video calls without using the Live Assist UI. A lot of the expected error handling has been omitted for clarity.

The demo app includes some platform-specific UI code (e.g. animations) that are only in place to highlight how configurable your UI can be.

Please note that the demo uses a simple app architecture that may not be suitable for a larger, production-ready app. Please make your own decisions around app architecture to suit your needs.
