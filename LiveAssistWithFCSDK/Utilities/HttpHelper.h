#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HttpHelper : NSObject

- (void)makeJsonRequestWithUrlString:(NSString *)urlString
                              method:(NSString *)method
                            bodyData:(NSData *)bodyData
                   completionHandler:(void (^)(NSDictionary * _Nullable))completionHandler;

- (void)makeJsonRequestWithUrlString:(NSString *)urlString
                              method:(NSString *)method
                            bodyJson:(NSDictionary *)bodyJson
                   completionHandler:(void (^)(NSDictionary * _Nullable))completionHandler;

- (void)makeJsonRequestWithUrlString:(NSString *)urlString
                              method:(NSString *)method
                   completionHandler:(void (^)(NSDictionary * _Nullable))completionHandler;

@end

NS_ASSUME_NONNULL_END
