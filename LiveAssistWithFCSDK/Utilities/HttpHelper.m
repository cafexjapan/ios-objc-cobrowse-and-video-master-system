#import "HttpHelper.h"

NS_ASSUME_NONNULL_BEGIN

@implementation HttpHelper

- (void)makeJsonRequestWithUrlString:(NSString *)urlString
                              method:(NSString *)method
                            bodyData:(NSData *)bodyData
                   completionHandler:(void (^)(NSDictionary * _Nullable))completionHandler {
    [self makeJsonRequestWithUrlString:urlString
                                method:method
                                  body:bodyData
                     completionHandler:completionHandler];
}

- (void)makeJsonRequestWithUrlString:(NSString *)urlString
                              method:(NSString *)method
                            bodyJson:(NSDictionary *)bodyJson
                   completionHandler:(void (^)(NSDictionary * _Nullable))completionHandler {
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:bodyJson options:0 error:nil];
    [self makeJsonRequestWithUrlString:urlString
                                method:method
                                  body:bodyData
                     completionHandler:completionHandler];
}

- (void)makeJsonRequestWithUrlString:(NSString *)urlString
                              method:(NSString *)method
                   completionHandler:(void (^)(NSDictionary * _Nullable))completionHandler {
    [self makeJsonRequestWithUrlString:urlString
                                method:method
                                  body:nil
                     completionHandler:completionHandler];
}

- (void)makeJsonRequestWithUrlString:(NSString *)urlString
                              method:(NSString *)method
                                body:(nullable NSData *)body
                   completionHandler:(void (^)(NSDictionary * _Nullable jsonResponse))completionHandler {
    NSURLRequest *request = [self createUrlRequestWithUrlString:urlString method:method body:body];
    __weak typeof(self) weakSelf = self;
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [weakSelf processData:data completionHandler:completionHandler];
    }];

    [task resume];
}

- (NSURLRequest *)createUrlRequestWithUrlString:(NSString *)urlString
                                         method:(NSString *)method
                                           body:(nullable NSData *)body {
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if (body.length > 0) {
        [request setHTTPBody:body];
    }
    return [request copy];
}

- (void)processData:(nullable NSData *)data
  completionHandler:(void (^)(NSDictionary * _Nullable))completionHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (data.length > 0) {
            NSError *error = nil;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

            if (response != nil && error == nil) {
                completionHandler(response);
                return;
            }
        }
        
        completionHandler(nil);
    });
}

@end

NS_ASSUME_NONNULL_END
