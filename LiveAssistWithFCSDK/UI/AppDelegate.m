#import "AppDelegate.h"

#import "CallViewController.h"
#import "PermissionManager.h"
#import "RemoteAEDSessionTokenProvider.h"
#import "SessionManager.h"
#import "HttpHelper.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UIWindow *window = [UIWindow new];
    
    HttpHelper *httpHelper = [HttpHelper new];
    id<SessionTokenProvider> sessionTokenProvider = [[RemoteAEDSessionTokenProvider alloc] initWithHttpHelper:httpHelper];
    PermissionManager *permissionManager = [PermissionManager new];
    SessionManager *sessionManager = [[SessionManager alloc] initWithSessionTokenProvider:sessionTokenProvider];
    
    CallViewController *rootViewController = [[CallViewController alloc] initWithPermissionManager:permissionManager
                                                                                    sessionManager:sessionManager];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    
    window.rootViewController = navigationController;
    [window makeKeyAndVisible];
    
    self.window = window;
    return YES;
}

@end
