#import "UIView+KFA.h"

@implementation UIView (KFA)

- (void)cba_startSmallViewAnimations {
    [self cba_startSwayingWithMultiple:6 duration:4];
    [self cba_startPulsingWithDuration:2];
}

- (void)cba_startLargeViewAnimations {
    [self cba_startSwayingWithMultiple:12 duration:7];
    [self cba_startPulsingWithDuration:3.5];
}

- (void)cba_stopAnimations {
    [self cba_stopSwaying];
    [self cba_stopPulsing];
}

- (void)cba_startSwayingWithMultiple:(double)multiple duration:(CFTimeInterval)duration {
    [self cba_startAnimationWithKeypath:@"position.x" multiple:multiple duration:duration];
}

- (void)cba_stopSwaying {
    [self.layer removeAnimationForKey:@"position.x"];
}

- (void)cba_startPulsingWithDuration:(CFTimeInterval)duration {
    [self cba_startAnimationWithKeypath:@"transform.scale" multiple:0.025 duration:duration];
}

- (void)cba_stopPulsing {
    [self.layer removeAnimationForKey:@"transform.scale"];
}

- (void)cba_startAnimationWithKeypath:(NSString *)keypath
                             multiple:(double)multiple
                             duration:(CFTimeInterval)duration {
    // Start a simple KFA to prove we're in full control of the call UI...
    CAKeyframeAnimation *kfa = [CAKeyframeAnimation new];
    kfa.keyPath = keypath;
    kfa.values = [self cba_generateCosinesForStepCount:16 multiple:multiple];
    [kfa setAdditive:YES];
    kfa.repeatCount = HUGE;
    kfa.duration = duration;

    [self.layer addAnimation:kfa forKey:keypath];
}

- (NSArray<NSNumber *> *)cba_generateCosinesForStepCount:(int)stepCount multiple:(double)mutliple {
    NSMutableArray *array = [NSMutableArray new];
    double increment = 2.0 * M_PI / stepCount;
    for (int i = 0; i <= stepCount; i++) {
        double input = ((double)i * increment);
        double cosine = cos(input);
        [array addObject:@(cosine * mutliple)];
    }
    return [array copy];
}

@end
