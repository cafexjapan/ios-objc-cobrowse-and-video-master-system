#import "CallViewController.h"

#import "Configuration.h"
#import "MaskView.h"
#import "PermissionManager.h"
#import "SessionManager.h"
#import "UIKit+Style.h"
#import "UIView+KFA.h"
#import "WebViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CallViewController () <SessionManagerDelegate>

@property (nonatomic, strong) PermissionManager *permissionManager;
@property (nonatomic, strong) SessionManager *sessionManager;
@property (nonatomic, strong) UIButton *callButton;
@property (nonatomic, strong) UIButton *webButton;
@property (nonatomic, strong) MaskView *localVideoView;
@property (nonatomic, strong) MaskView *remoteVideoView;

@end

@implementation CallViewController

- (instancetype)initWithPermissionManager:(PermissionManager *)permissionManager
                           sessionManager:(SessionManager *)sessionManager {
    if (self = [super initWithNibName:nil bundle:nil]) {
        _permissionManager = permissionManager;
        _sessionManager = sessionManager;
        _callButton = [UIButton cba_createButton];
        _webButton = [UIButton cba_createButton];
        _localVideoView = [MaskView new];
        _remoteVideoView = [MaskView new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];

    _sessionManager.delegate = self;
    _sessionManager.localVideoView = _localVideoView;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_localVideoView cba_startSmallViewAnimations];
    [_remoteVideoView cba_startLargeViewAnimations];
    
    _sessionManager.remoteVideoView = _remoteVideoView;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [_localVideoView cba_stopAnimations];
    [_remoteVideoView cba_stopAnimations];
}

- (void)configureView {
    _localVideoView.translatesAutoresizingMaskIntoConstraints = NO;
    _remoteVideoView.translatesAutoresizingMaskIntoConstraints = NO;

    [_callButton setTitle:@"Make Call" forState:UIControlStateNormal];
    [_callButton setTitle:@"Connecting..." forState:UIControlStateDisabled];
    [_callButton addTarget:self action:@selector(callButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    [_webButton setTitle:@"To Web >>>" forState:UIControlStateNormal];
    [_webButton addTarget:self action:@selector(webButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_remoteVideoView];
    [self.view addSubview:_localVideoView];
    [self.view addSubview:_callButton];
    [self.view addSubview:_webButton];

    UILayoutGuide *target = self.view.safeAreaLayoutGuide;
    NSArray<NSLayoutConstraint *> *constraints = @[
        [_callButton.topAnchor constraintEqualToAnchor:target.topAnchor constant:16],
        [_callButton.leadingAnchor constraintEqualToAnchor:target.leadingAnchor constant:16],

        [_webButton.topAnchor constraintEqualToAnchor:target.topAnchor constant:16],
        [_webButton.trailingAnchor constraintEqualToAnchor:target.trailingAnchor constant:-16],

        [_remoteVideoView.leadingAnchor constraintEqualToAnchor:target.leadingAnchor constant:16],
        [_remoteVideoView.trailingAnchor constraintEqualToAnchor:target.trailingAnchor constant:-16],
        [_remoteVideoView.topAnchor constraintEqualToAnchor:target.topAnchor constant:16],
        [_remoteVideoView.bottomAnchor constraintEqualToAnchor:target.bottomAnchor constant:-16],

        [_localVideoView.trailingAnchor constraintEqualToAnchor:_remoteVideoView.trailingAnchor constant:-16],
        [_localVideoView.bottomAnchor constraintEqualToAnchor:_remoteVideoView.bottomAnchor constant:-16],
        [_localVideoView.widthAnchor constraintEqualToAnchor:_remoteVideoView.widthAnchor multiplier:0.2],
        [_localVideoView.heightAnchor constraintEqualToAnchor:_remoteVideoView.heightAnchor multiplier:0.2],
    ];

    [NSLayoutConstraint activateConstraints:constraints];
}

- (void)callButtonTapped:(UIButton *)button {
    __weak typeof(self) weakSelf = self;
    [_permissionManager requestRequiredPermissionsWithCompletion:^(BOOL granted) {
        if (granted) {
            [weakSelf makeCall];
        }
    }];
}

- (void)makeCall {
    [_callButton setEnabled:NO];
    if (_sessionManager.hasCurrentCall) {
        [_sessionManager endCall];
    } else {
        [_sessionManager createCallToDestination:[Configuration callee]];
    }
}

- (void)webButtonTapped:(UIButton *)button {
    WebViewController *nextViewController = [[WebViewController alloc] initWithSessionManager:_sessionManager];
    [self.navigationController pushViewController:nextViewController animated:YES];
}

#pragma mark - SessionManagerDelegate

- (void)sessionManagerDidStartCall:(SessionManager *)sessionManager {
    [_callButton setTitle:@"End Call" forState:UIControlStateNormal];
    [_callButton setEnabled:YES];
}

- (void)sessionManagerCallDidFail:(SessionManager *)sessionManager {
    NSLog(@"Call did fail");
    [_callButton setEnabled:YES];
}

- (void)sessionManagerDidEndCall:(SessionManager *)sessionManager {
    [_callButton setTitle:@"Make Call" forState:UIControlStateNormal];
    [_callButton setEnabled:YES];
}

@end

NS_ASSUME_NONNULL_END
