#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// Some simple KFAs to prove we're in full control of the call UI...
@interface UIView (KFA)

- (void)cba_startSmallViewAnimations;
- (void)cba_startLargeViewAnimations;
- (void)cba_stopAnimations;

@end

NS_ASSUME_NONNULL_END
