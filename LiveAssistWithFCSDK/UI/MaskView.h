#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// A view that draws the largest possible circular mask around the centerpoint, staying entirely in the visible frame.
@interface MaskView : UIView

@end

NS_ASSUME_NONNULL_END
