#import "MaskView.h"

NS_ASSUME_NONNULL_BEGIN

@implementation MaskView

- (void)layoutSubviews {
    [super layoutSubviews];

    CAShapeLayer *circle = [CAShapeLayer layer];
    CGRect centralSquare = [self centralSquare];
    circle.path = [[UIBezierPath bezierPathWithRoundedRect:centralSquare cornerRadius:centralSquare.size.width / 2] CGPath];
    circle.fillColor = [UIColor blackColor].CGColor;
    self.layer.mask = circle;
}

- (CGRect)centralSquare {
    CGSize currentSize = self.bounds.size;
    CGFloat smallestDimension = MIN(currentSize.width, currentSize.height);
    return CGRectMake((currentSize.width / 2) - (smallestDimension / 2),
                      (currentSize.height / 2) - (smallestDimension / 2),
                      smallestDimension,
                      smallestDimension);
}

@end

NS_ASSUME_NONNULL_END
