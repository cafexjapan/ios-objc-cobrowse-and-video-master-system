#import <UIKit/UIKit.h>

@class SessionManager;

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : UIViewController

+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithSessionManager:(SessionManager *)sessionManager NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
