#import "WebViewController.h"

#import <WebKit/WebKit.h>

#import "MaskView.h"
#import "SessionManager.h"
#import "UIView+KFA.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController () <WKNavigationDelegate>

@property (nonatomic, strong) SessionManager *sessionManager;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) MaskView *remoteVideoView;

@end

@implementation WebViewController

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager {
    if (self = [super initWithNibName:nil bundle:nil]) {
        _sessionManager = sessionManager;
        _webView = [WKWebView new];
        _webView.navigationDelegate = self;
        _remoteVideoView = [MaskView new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    NSURL *url = [NSURL URLWithString:@"https://webrtc.org/"];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [_webView loadRequest:request];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_remoteVideoView cba_startSmallViewAnimations];

    _sessionManager.remoteVideoView = _remoteVideoView;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [_remoteVideoView cba_stopAnimations];
}

- (void)configureView {
    _webView.translatesAutoresizingMaskIntoConstraints = NO;
    _remoteVideoView.translatesAutoresizingMaskIntoConstraints = NO;
        
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_webView];
    [self.view addSubview:_remoteVideoView];

    UILayoutGuide *target = self.view.safeAreaLayoutGuide;
    NSArray<NSLayoutConstraint *> *constraints = @[
        [_webView.leadingAnchor constraintEqualToAnchor:target.leadingAnchor],
        [_webView.trailingAnchor constraintEqualToAnchor:target.trailingAnchor],
        [_webView.topAnchor constraintEqualToAnchor:target.topAnchor],
        [_webView.bottomAnchor constraintEqualToAnchor:target.bottomAnchor],

        [_remoteVideoView.trailingAnchor constraintEqualToAnchor:target.trailingAnchor constant:-16],
        [_remoteVideoView.bottomAnchor constraintEqualToAnchor:target.bottomAnchor constant:-16],
        [_remoteVideoView.widthAnchor constraintEqualToAnchor:target.widthAnchor multiplier:0.2],
        [_remoteVideoView.heightAnchor constraintEqualToAnchor:target.heightAnchor multiplier:0.2],
    ];

    [NSLayoutConstraint activateConstraints:constraints];
}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
}

@end

NS_ASSUME_NONNULL_END
