#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (CBAStyle)

+ (UIButton *)cba_createButton;

@end

NS_ASSUME_NONNULL_END
