#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SessionTokenProvider

- (void)provideSessionTokenWithCompletionHandler:(void (^)(NSString * _Nullable))completionHandler;

@end

NS_ASSUME_NONNULL_END
