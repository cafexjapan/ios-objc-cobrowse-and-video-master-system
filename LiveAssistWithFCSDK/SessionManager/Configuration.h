#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Configuration : NSObject

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

+ (NSString *)authenticationUrl;
+ (NSString *)gatewayScheme;
+ (NSString *)gatewayAddress;
+ (NSString *)gatewayPort;
+ (NSString *)gatewayUrl;
+ (NSString *)caller;
+ (NSString *)callee;

@end

NS_ASSUME_NONNULL_END
