#import "RemoteAEDSessionTokenProvider.h"

#import "Configuration.h"
#import "HttpHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface RemoteAEDSessionTokenProvider ()

@property (nonatomic, strong) HttpHelper *httpHelper;

@end

@implementation RemoteAEDSessionTokenProvider

- (instancetype)initWithHttpHelper:(HttpHelper *)httpHelper {
    if (self = [super init]) {
        _httpHelper = httpHelper;
    }
    return self;
}

- (void)provideSessionTokenWithCompletionHandler:(void (^)(NSString * _Nullable))completionHandler {
    NSString *urlString = [NSString stringWithFormat:@"%@&userId=%@", [Configuration authenticationUrl], [Configuration caller]];

    [_httpHelper makeJsonRequestWithUrlString:urlString
                                       method:@"GET"
                            completionHandler:^(NSDictionary* _Nullable jsonResponse) {
        completionHandler([jsonResponse objectForKey:@"sessionid"]);
    }];
}

@end

NS_ASSUME_NONNULL_END
