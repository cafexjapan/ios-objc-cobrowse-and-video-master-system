#import "SessionManager.h"

#import <AssistSDK.h>
#import <ACBClientSDK/ACBClientSDK.h>

#import "Configuration.h"
#import "SessionTokenProvider.h"

@interface SessionManager () <ACBUCDelegate, ACBClientCallDelegate>

@property (nonatomic, strong) id<SessionTokenProvider> sessionTokenProvider;

@property (nonatomic, strong, nullable) NSString *sessionToken;
@property (nonatomic, strong, nullable) NSString *destination;
@property (nonatomic, strong, nullable) ACBUC *uc;
@property (nonatomic, strong, nullable) ACBClientCall *currentCall;

@end

@implementation SessionManager

- (instancetype)initWithSessionTokenProvider:(id<SessionTokenProvider>)sessionTokenProvider {
    if (self = [super init]) {
        _sessionTokenProvider = sessionTokenProvider;
    }
    return self;
}

- (BOOL)hasCurrentCall {
    return _currentCall != nil;
}

- (void)setLocalVideoView:(UIView *)localVideoView {
    _localVideoView = localVideoView;
    [_uc.phone setPreviewView:localVideoView];
}

- (void)setRemoteVideoView:(UIView *)remoteVideoView {
    _remoteVideoView = remoteVideoView;
    _currentCall.videoView = remoteVideoView;
}

- (void)createCallToDestination:(NSString *)destination {
    _destination = destination;
    
    __weak typeof(self) weakSelf = self;
    [_sessionTokenProvider provideSessionTokenWithCompletionHandler:^(NSString * _Nullable sessionToken) {
        [weakSelf startSessionWithSessionToken:sessionToken];
    }];
}

- (void)startSessionWithSessionToken:(NSString *)sessionToken {
    _sessionToken = sessionToken;
    _uc = [ACBUC ucWithConfiguration:sessionToken delegate:self];
    [_uc setNetworkReachable:YES]; // Naive assumption
    [_uc.phone setPreviewView:_localVideoView];
    [_uc startSession];
}

- (void)createCall {
    _currentCall = [self.uc.phone createCallToAddress: _destination
                                            withAudio: ACBMediaDirectionSendAndReceive
                                                video: ACBMediaDirectionSendAndReceive
                                             delegate: self];

    _currentCall.videoView = _remoteVideoView;
}

- (BOOL)updateRemoteVideoViewForCurrentCall:(UIView *)remoteVideoView {
    if (!self.hasCurrentCall) {
        return false;
    }
    
    _currentCall.videoView = remoteVideoView;
    return true;
}

- (void)endCall {
    [_currentCall end];
}

- (void)startCoBrowse {
    NSDictionary *supportParameters = @{
        @"sessionToken": _sessionToken,
        @"correlationId": [Configuration caller]
    };
    [AssistSDK startSupport:[Configuration gatewayUrl] supportParameters:supportParameters];
}

#pragma mark - ACBUCDelegate

- (void)ucDidFailToStartSession:(ACBUC *)uc {
    NSLog(@"Unable to start session");
    [self reportAndTearDownCall];
}

- (void)ucDidLoseConnection:(ACBUC *)uc {
    NSLog(@"Connection lost");
    [self reportAndTearDownCall];
}

- (void)ucDidReceiveSystemFailure:(ACBUC *)uc {
    NSLog(@"System failure");
    [self reportAndTearDownCall];
}

- (void)ucDidStartSession:(ACBUC *)uc {
    [self createCall];
}

- (void)reportAndTearDownCall {
    [_delegate sessionManagerCallDidFail:self];
    [self endCall];
}

#pragma mark - ACBClientCallDelegate

- (void)call:(ACBClientCall *)call didChangeStatus:(ACBClientCallStatus)status {
    NSLog(@"didChangeStatus: %d", status);
    switch (status) {
        case ACBClientCallStatusInCall:
            [self startCoBrowse];
            [_delegate sessionManagerDidStartCall:self];
            break;
        case ACBClientCallStatusEnded:
            _currentCall = nil;
            [_uc stopSession];
            [AssistSDK endSupport];
            [_delegate sessionManagerDidEndCall:self];
            break;
        case ACBClientCallStatusTimedOut:
        case ACBClientCallStatusBusy:
        case ACBClientCallStatusNotFound:
        case ACBClientCallStatusError:
            _currentCall = nil;
            [_delegate sessionManagerCallDidFail:self];
            break;
        default:
            break;
    }
}

- (void)callDidReceiveMediaChangeRequest:(ACBClientCall *)call {
    NSLog(@"callDidReceiveMediaChangeRequest");
}

@end
