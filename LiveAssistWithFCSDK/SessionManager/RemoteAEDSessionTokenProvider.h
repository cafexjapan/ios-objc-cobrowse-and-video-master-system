#import <Foundation/Foundation.h>
#import "SessionTokenProvider.h"

@class HttpHelper;

NS_ASSUME_NONNULL_BEGIN

@interface RemoteAEDSessionTokenProvider : NSObject<SessionTokenProvider>

+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithHttpHelper:(HttpHelper *)httpHelper NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
