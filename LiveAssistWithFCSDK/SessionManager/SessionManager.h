#import <UIKit/UIKit.h>

@protocol SessionTokenProvider;
@class SessionManager;

NS_ASSUME_NONNULL_BEGIN

@protocol SessionManagerDelegate

- (void) sessionManagerDidStartCall:(SessionManager *)sessionManager;
- (void) sessionManagerCallDidFail:(SessionManager *)sessionManager;
- (void) sessionManagerDidEndCall:(SessionManager *)sessionManager;

@end

@interface SessionManager : NSObject

@property (nonatomic, weak, nullable) id<SessionManagerDelegate> delegate;
@property (nonatomic, strong, nullable) UIView *localVideoView;
@property (nonatomic, strong, nullable) UIView *remoteVideoView;
@property (nonatomic, readonly) BOOL hasCurrentCall;

+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithSessionTokenProvider:(id<SessionTokenProvider>)sessionTokenProvider NS_DESIGNATED_INITIALIZER;

- (void)createCallToDestination:(NSString *)destination;
- (void)endCall;

@end

NS_ASSUME_NONNULL_END
