// Configuration is currently pulled from a file called `Secrets.h` which defines the actual
// values as `#define` preprocessor directives.
// This file has not been included in the repository for security purposes.
//
// e.g.
//
//#define AUTHENTICATION_URL @"https://your.authentication.address/path/to/service"
//#define GATEWAY_SCHEME @"https"
//#define GATEWAY_ADDRESS @"your.gateway.address"
//#define GATEWAY_PORT @"8443"
//#define GATEWAY_CALLER @"assist-anon"
//#define GATEWAY_CALLEE @"agent1"

#import "Configuration.h"

#import "Secrets.h"

NS_ASSUME_NONNULL_BEGIN

@implementation Configuration

+ (NSString *)authenticationUrl {
    return AUTHENTICATION_URL;
}

+ (NSString *)gatewayScheme {
    return GATEWAY_SCHEME;
}

+ (NSString *)gatewayAddress {
    return GATEWAY_ADDRESS;
}

+ (NSString *)gatewayPort {
    return GATEWAY_PORT;
}

+ (NSString *)gatewayUrl {
    return [NSString stringWithFormat:@"%@://%@:%@", [self gatewayScheme], [self gatewayAddress], [self gatewayPort]];
}

+ (NSString *)caller {
    return GATEWAY_CALLER;
}

+ (NSString *)callee {
    return GATEWAY_CALLEE;
}

@end

NS_ASSUME_NONNULL_END
