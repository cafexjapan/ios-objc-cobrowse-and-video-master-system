#import "PermissionManager.h"

#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AVCaptureDevice (Extensions)

+ (void)requestAccessForMediaTypeOnMainThread:(AVMediaType)mediaType completionHandler:(void (^)(BOOL))handler;

@end

@implementation AVCaptureDevice (Extensions)

+ (void)requestAccessForMediaTypeOnMainThread:(AVMediaType)mediaType completionHandler:(void (^)(BOOL))handler {
    [self requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        dispatch_async(dispatch_get_main_queue(), ^{
            handler(granted);
        });
    }];
}

@end

@implementation PermissionManager

- (void)requestRequiredPermissionsWithCompletion:(void (^)(BOOL))completion {
    [AVCaptureDevice requestAccessForMediaTypeOnMainThread:AVMediaTypeAudio completionHandler:^(BOOL granted) {
        if (!granted) {
            completion(false);
            return;
        }
        [AVCaptureDevice requestAccessForMediaTypeOnMainThread:AVMediaTypeVideo completionHandler:completion];
    }];
}

@end

NS_ASSUME_NONNULL_END
