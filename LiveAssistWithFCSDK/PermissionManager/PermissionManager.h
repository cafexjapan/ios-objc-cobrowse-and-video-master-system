#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PermissionManager : NSObject

- (void)requestRequiredPermissionsWithCompletion:(void (^)(BOOL))completion;

@end

NS_ASSUME_NONNULL_END
